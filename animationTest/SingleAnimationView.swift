//
//  SingleAnimationView.swift
//  animation
//
//  Created by Mohamad Tarabishi on 1/9/22.
//

import UIKit
protocol SingleAnimationViewDelegateAction {
    func viewOpend()
}
class SingleAnimationView : UIView{
    var isPossible : Bool = true
    var isOpend : Bool = false
    var beginPressTime = CACurrentMediaTime()
    var parentView : UIViewController?  = nil
    var mainView = UIVisualEffectView()
    var delegate :SingleAnimationViewDelegateAction? = nil
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        // Do any additional setup after loading the view.
        setupViews()
    }
    func setSuperViewCenter(parentView: UIViewController){
        self.parentView = parentView
        let blurEffect = UIBlurEffect(style: .regular)
        let view = UIVisualEffectView(effect: blurEffect)
        view.center = parentView.view.center
        let height = CGFloat(175.0 * 2.5)
        let width = CGFloat(350)

        view.frame = CGRect(x: view.frame.minX - width/2, y: view.frame.minY - height/2, width: width, height: height)
        mainView = view
        self.mainView.layer.cornerRadius = 30
        self.mainView.clipsToBounds = true
        self.mainView.backgroundColor = .clear
        self.mainView.alpha = 0
        self.parentView?.view.addSubview(view)
    }
    func setupViews()  {
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(gridViewLongPressed))
        lpgr.minimumPressDuration = 0
        self.addGestureRecognizer(lpgr)
        self.layer.cornerRadius = 25
    }
    @objc func gridViewLongPressed(_ sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .began:
            print("began")
            startAnimation()
            break
        case .changed:
            print("changed")
            checkIfEnd()
            break
        case .ended:
            checkIfEnd()
            break
        case .possible:
            print("possible")

            break
        case .cancelled:
            checkIfEnd()
            break
        case .failed:
            checkIfEnd()
            break
        @unknown default:
            break
        }
    }
    func checkIfEnd()  {
        print("ended")
        let deltaTime = CACurrentMediaTime() - beginPressTime
        print("deltaTime :\(deltaTime)")
        if deltaTime < 0.3{
            isPossible = false
            endAnimation()
        }
    }
    func startAnimation()  {
        if !isOpend{
            self.parentView?.view.addSubview(mainView)
            beginPressTime = CACurrentMediaTime()
            isPossible = true
            addAnimation(layer)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if self.isPossible {
                    let generator = UIImpactFeedbackGenerator(style: .heavy)
                                generator.impactOccurred()
                    self.mainView.alpha = 1
                    self.addAnimation(self.mainView.layer)
                    self.isOpend = true
                    self.delegate?.viewOpend()
                }
            }
        }
    }
    func endAnimation(){
        layer.removeAnimation(forKey: "pop")
        self.mainView.layer.removeAnimation(forKey: "pop")
        self.mainView.alpha = 0
        mainView.removeFromSuperview()
        self.isOpend = false
    }

    func addAnimation(_ layer:CALayer)  {
        let force = 0.7
        let animation = CAKeyframeAnimation()
        animation.keyPath = "transform.scale"
        animation.values = [0, -0.1*force,-0.2*force]
        animation.keyTimes = [0, 0.2,0.6,0.8, 1]
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        animation.duration = CFTimeInterval(0.3)
        animation.isAdditive = true
        animation.repeatCount = 1
        animation.beginTime = CACurrentMediaTime() + CFTimeInterval(0.0)
        layer.add(animation, forKey: "pop")
    }
}
