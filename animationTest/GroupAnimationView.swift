//
//  GroupAnimationView.swift
//  animation
//
//  Created by Mohamad Tarabishi on 1/9/22.
//

import UIKit

class GroupAnimationView : UIView{
    @IBOutlet weak var the1SingleView: SingleAnimationView!
    @IBOutlet weak var the2SingleView: SingleAnimationView!
    @IBOutlet weak var the3SingleView: SingleAnimationView!
    @IBOutlet weak var the4SingleView: SingleAnimationView!
    @IBOutlet weak var the5SingleView: SingleAnimationView!
    @IBOutlet weak var the6SingleView: SingleAnimationView!
    @IBOutlet weak var the1SingleViewContainer: UIView!
    @IBOutlet weak var the2SingleViewContainer: UIView!
    @IBOutlet weak var the3SingleViewContainer: UIView!
    @IBOutlet weak var the4SingleViewContainer: UIView!
    @IBOutlet weak var the5SingleViewContainer: UIView!
    @IBOutlet weak var the6SingleViewContainer: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    

    var isPossible : Bool = true
    var isOpend : Bool = false
    var beginPressTime = CACurrentMediaTime()
    var xScaleFactor :CGFloat = 2
    var yScaleFactor :CGFloat = 2.5
    var superCenter : CGPoint  = CGPoint (x: 0, y: 0)
    var parentView : UIViewController?  = nil

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        // Do any additional setup after loading the view.

    }
    func setSuperViewCenter(parentView: UIViewController){
        self.parentView = parentView
        superCenter = parentView.view.center
        guard let parentView = self.parentView else {
            return
        }
        the1SingleView.setSuperViewCenter(parentView: parentView )
        the1SingleView.delegate = self
        
        the2SingleView.setSuperViewCenter(parentView: parentView )
        the2SingleView.delegate = self

        the3SingleView.setSuperViewCenter(parentView: parentView )
        the3SingleView.delegate = self

        the4SingleView.setSuperViewCenter(parentView: parentView )
        the4SingleView.delegate = self

        the5SingleView.setSuperViewCenter(parentView: parentView )
        the5SingleView.delegate = self

        the6SingleView.setSuperViewCenter(parentView: parentView )
        the6SingleView.delegate = self
        
        setupViews()
    }
    func setupViews()  {
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(gridViewLongPressed))
        lpgr.minimumPressDuration = 0
        self.addGestureRecognizer(lpgr)
        self.layer.cornerRadius = 10
        ViewsInteractions(false)
    }
    @objc func gridViewLongPressed(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            print("began")
            startAnimation()
            break
        case .changed:
            print("changed")
            checkIfEnd()
            break
        case .ended:
            checkIfEnd()
            break
        case .possible:
            print("possible")

            break
        case .cancelled:
            checkIfEnd()
            break
        case .failed:
            checkIfEnd()
            break
        @unknown default:
            break
        }
    }
    func checkIfEnd()  {
        print("ended")
        let deltaTime = CACurrentMediaTime() - beginPressTime
        print("deltaTime :\(deltaTime)")
        if deltaTime < 0.3{
            isPossible = false
            endAnimation()
        }
    }
    func startAnimation()  {
        if !isOpend{
            beginPressTime = CACurrentMediaTime()
            isPossible = true
            addAnimation(0.3)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if self.isPossible {
                    let generator = UIImpactFeedbackGenerator(style: .heavy)
                    generator.impactOccurred()
                    UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseIn) {
                        let xDestination = self.superCenter.x/self.xScaleFactor - (self.center.x/self.xScaleFactor)
                        let YDestination = self.superCenter.y/self.yScaleFactor - (self.center.y/self.yScaleFactor)
                        var translate = CGAffineTransform(translationX: xDestination, y: YDestination)
                        var scale = CGAffineTransform(scaleX: self.xScaleFactor, y: self.yScaleFactor)
                        let translateAndScale = translate.concatenating(scale)
                        self.transform = translateAndScale
                        scale = CGAffineTransform(scaleX: 0.5, y: 0.5)
                        translate = CGAffineTransform(translationX: self.the1SingleViewContainer.center.x/(self.xScaleFactor*2), y: -(self.the1SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the1SingleViewContainer.transform = translate.concatenating(scale)
                        translate = CGAffineTransform(translationX: 0, y: -(self.the2SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the2SingleViewContainer.transform = translate.concatenating(scale)
                        translate = CGAffineTransform(translationX: self.the3SingleViewContainer.center.x/(self.xScaleFactor*2), y: -(self.the3SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the3SingleViewContainer.transform = translate.concatenating(scale)
                        translate = CGAffineTransform(translationX: 0, y: -(self.the4SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the4SingleViewContainer.transform = translate.concatenating(scale)
                        translate = CGAffineTransform(translationX: self.the5SingleViewContainer.center.x/(self.xScaleFactor*2), y: -(self.the5SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the5SingleViewContainer.transform = translate.concatenating(scale)
                        translate = CGAffineTransform(translationX: 0, y: -(self.the6SingleViewContainer.center.y/self.yScaleFactor*2))

                        self.the6SingleViewContainer.transform = translate.concatenating(scale)
                        
                        self.layoutSubviews()
                        self.layer.cornerRadius = 15
                    } completion: { (_) in
                        self.isOpend = true
                        self.ViewsInteractions(true)

                    }
                }
            }
        }
    }
    func endAnimation(){
        guard !the1SingleView.isOpend , !the2SingleView.isOpend  ,!the3SingleView.isOpend  ,!the4SingleView.isOpend  ,!the5SingleView.isOpend,!the6SingleView.isOpend else{
            endViewsAnimation()
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
                self.addAnimation(0.2)
                self.alpha = 1
            } completion: { (_) in
                
            }
            return
        }
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseIn) {
            let translate = CGAffineTransform(translationX: 0, y: 0)
            let scale = CGAffineTransform(scaleX: 1, y: 1)
            let translateAndScale = translate.concatenating(scale)
            self.transform = translateAndScale
            self.the1SingleViewContainer.transform = translateAndScale
            self.the2SingleViewContainer.transform = translateAndScale
            self.the3SingleViewContainer.transform = translateAndScale
            self.the4SingleViewContainer.transform = translateAndScale
            self.the5SingleViewContainer.transform = translateAndScale
            self.the6SingleViewContainer.transform = translateAndScale
            self.layer.cornerRadius = 10
        } completion: { (_) in
            self.isOpend = false
            self.ViewsInteractions(false)
        }
    }

    func addAnimation(_ duration: Double)  {
        let force = 0.7
        let animation = CAKeyframeAnimation()
        animation.keyPath = "transform.scale"
        animation.values = [0, -0.1*force,-0.2*force]
        animation.keyTimes = [0, 0.2,0.6,0.8, 1]
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        animation.duration = CFTimeInterval(duration)
        animation.isAdditive = true
        animation.repeatCount = 1
        animation.beginTime = CACurrentMediaTime() + CFTimeInterval(0.0)
        layer.add(animation, forKey: "pop")
    }
    func applyTransformsToContainers(){
        
    }
    func endViewsAnimation(){
        the1SingleView.endAnimation()
        the2SingleView.endAnimation()
        the3SingleView.endAnimation()
        the4SingleView.endAnimation()
        the5SingleView.endAnimation()
        the6SingleView.endAnimation()
    }
    func ViewsInteractions(_ isEnable:Bool){
        the1SingleView.isUserInteractionEnabled = isEnable
        the2SingleView.isUserInteractionEnabled = isEnable
        the3SingleView.isUserInteractionEnabled = isEnable
        the4SingleView.isUserInteractionEnabled = isEnable
        the5SingleView.isUserInteractionEnabled = isEnable
        the6SingleView.isUserInteractionEnabled = isEnable
    }
}
extension GroupAnimationView :SingleAnimationViewDelegateAction{
    func viewOpend() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
            self.alpha = 0
        } completion: { (_) in
            
        }

    }
}
