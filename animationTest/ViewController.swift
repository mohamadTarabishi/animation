//
//  ViewController.swift
//  animation
//
//  Created by Mohamad Tarabishi on 1/9/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var gridView: GroupAnimationView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
    }
    func setupViews()  {
        gridView.setSuperViewCenter(parentView: self)
    }

    @IBAction func touchBackground(_ sender: AnyObject) {
        gridView.endAnimation()
    }

}
